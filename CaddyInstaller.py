import subprocess

class CaddyInstaller:

    def run():
        subprocess.run("sudo apt install -y debian-keyring debian-archive-keyring apt-transport-https")
        subprocess.run("sudo apt install -y debian-keyring debian-archive-keyring apt-transport-https")
        subprocess.run("curl -1sLf 'https://dl.cloudsmith.io/public/caddy/stable/gpg.key' | sudo apt-key add -")
        subprocess.run("curl -1sLf 'https://dl.cloudsmith.io/public/caddy/stable/debian.deb.txt' | sudo tee -a /etc/apt/sources.list.d/caddy-stable.list")
        subprocess.run("sudo apt update")
        subprocess.run("sudo apt install caddy")
