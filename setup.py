#!/usr/bin/env python3

import CaddyInstaller

# Install caddy
caddyInstaller = CaddyInstaller()
caddyInstaller.run()